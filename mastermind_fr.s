; VIA
PORTB = $6000
PORTA = $6001
DDRB = $6002
DDRA = $6003

; LCD
E  = %10000000
RW = %01000000
RS = %00100000 

; UART
ACIA_DATA = $5000
ACIA_STATUS = $5001
ACIA_CMD = $5002
ACIA_CTRL = $5003

; BINARY
value = $0200      ; Carry - mod10 - value
mod10 = $0202      ; 2 bytes 
message_binary_buffer = $0204    ; 6 bytes

; GAME
MIN_LANES = 3
MAX_LANES = 9
MIN_USABLE_NUMBER = 1
MAX_USABLE_NUMBER = 9
; GAME MEMORY
message_to_print_lo = $10 ; 1 byte, contains the low order byte adress of any null-terminated message to print
message_to_print_ho = $11 ; 1 byte, high order
game_state = $020a  ; 1 byte: 0 is waiting for the number of lanes   (3-9)
                    ;         1 is waiting for the max usable number (1-9)
                    ;         2 is playing the game
                    ;         3 is having won/waiting to play a new game
lane_number = $020b ; 1 byte
usable_number = $020c ; 1 byte
received_before_cr = $0210  ; {lane_number} bytes max
                            ; it contains the sequence of the numbers entered by the user
                            ; for example : 6 5 0 2
received_char_pointer = $0219
secret_number = $0220   ; {lane_number} bytes
try_count = $0229   ; 1 byte
number_correct_numbers = $022a  ; 1 byte
number_almost_correct_numbers = $022b ; 1 byte

; RNG
seed = $022c ; 1 byte
has_rng_been_decided = $022d ; 1 byte, 0 = no
buffer = $022e ; 1 byte

; KEYS
key_cr = 13
key_zero = "0"

    .org $8000

reset:
    sei
    ldx #$ff
    txs            ; transfert de x au stack pointer 

    jsr init_via
    jsr init_lcd_display

    jsr print_startmsg_lcd
    jsr init_6551

    stz has_rng_been_decided

    ldx #>startmsg_serial
    ldy #<startmsg_serial
    jsr print_msg_serial 

    stz received_char_pointer  
    stz try_count

    ; waiting for lane number
reset_lane_number:
    stz game_state
    lda #9
    sta lane_number ; by default, for rx_wait to work
    ldx #>msg_pick_lane_number
    ldy #<msg_pick_lane_number
    jsr print_msg_serial
    jmp rx_wait

reset_max_number:
    lda #1
    sta game_state
    lda #9
    sta usable_number
    ldx #>msg_pick_max_number
    ldy #<msg_pick_max_number
    jsr print_msg_serial
    jmp rx_wait

rx_wait:
    inc seed
    lda ACIA_STATUS
    and #$08    ; data received register
    beq rx_wait    
    ; a byte has been received    
    lda ACIA_DATA
    ; cr ?
    cmp #13
    beq received_cr
    ; backspace ?
    cmp #8
    beq received_del

    jsr print_char_serial   ; echo
    sbc #"0"
    ; update received characters 
    ldx received_char_pointer 
    sta received_before_cr, x 
    ; update pointer if not too many characters have been sent
    ldx lane_number
    cpx received_char_pointer
    beq rx_wait
    inc received_char_pointer
    jmp rx_wait

received_del:
    jsr print_char_serial
    lda received_char_pointer
    beq rx_wait 

    dec received_char_pointer
    jmp rx_wait      

reset_lane_number_checkpoint:
    jmp reset_lane_number

reset_max_number_checkpoint:
    jmp reset_max_number

received_cr:
    lda received_char_pointer
    beq rx_wait

    stz received_char_pointer

    lda game_state
    cmp #0
    ; lane_number
    beq received_lane_number

    lda game_state
    cmp #1
    ; max_number
    beq received_max_number

    lda game_state
    cmp #2
    ; play phase
    jsr number_played   
 
    jmp rx_wait

received_wrong_number:
    stz received_char_pointer
    stz received_before_cr
    
    ldx #>msg_picked_wrong_number    
    ldy #<msg_picked_wrong_number    
    jsr print_msg_serial

    lda game_state
    beq reset_lane_number_checkpoint

    cmp #1
    beq reset_max_number_checkpoint

    jmp rx_wait

received_lane_number:
    ; check if the lane number is correct
    ; should be between MIN_LANE_NUMBER and MAX_LANE_NUMBER
    pha
    lda #13
    jsr print_char_serial 
    pla

    jsr print_char_serial
    lda received_before_cr
    sec     
    sbc #3
    bmi received_wrong_number

    ; doesn't work yet
    lda received_before_cr     
    clc
    adc #246
    bvs received_wrong_number

    lda #1
    sta game_state
    lda received_before_cr
    sta lane_number

    ; print choice
    ldx #>msg_picked_lane_number
    ldy #<msg_picked_lane_number
    jsr print_msg_serial    
    lda received_before_cr
    jsr print_number_serial
    lda #13
    jsr print_char_serial 

    jmp reset_max_number

received_max_number:
    ; check if the max number is correct
    ; should be between MIN_USABLE_NUMBER and MAX_USABLE_NUMBER
    pha
    lda #13
    jsr print_char_serial 
    pla

    jsr print_char_serial
    lda received_before_cr
    sec     
    sbc #1
    bmi received_wrong_number

    ; doesn't work yet
    lda received_before_cr     
    clc
    adc #246
    bvs received_wrong_number

    lda #2
    sta game_state
    lda received_before_cr
    sta usable_number

    ; print choice
    ldx #>msg_picked_max_number
    ldy #<msg_picked_max_number
    jsr print_msg_serial    
    lda received_before_cr
    jsr print_number_serial
    lda #13
    jsr print_char_serial 

    jsr pick_secret_number
    jsr print_try_count

    jmp rx_wait

find_random_number:
    phy
    lda seed
    adc #81
    sta seed
find_number_loop:
    inc seed
    inc
    and #%1111  ; max 15
    tay
    sec
    sbc usable_number
    beq its_zero
    bpl find_number_loop    ; too high

 its_zero: 
    lda seed
    adc #137
    sta seed 
     
    tya
    ply
    rts 

pick_secret_number:
    inc has_rng_been_decided
    pha
    phx
    phy

    jsr find_random_number      
    sta secret_number
    
    ldy #1
pick_secret_number_loop:
    jsr find_random_number
    sta secret_number, y
    ldx #0
has_been_picked_loop:
    lda secret_number, y
    cmp secret_number, x
    beq pick_secret_number_loop
    inx
    tya
    sta buffer
    cpx buffer 
    bne has_been_picked_loop
    iny
    cpy lane_number
    bne pick_secret_number_loop

    ldx #>msg_picked_secret_number
    ldy #<msg_picked_secret_number
    jsr print_msg_serial

    pla
    plx
    ply
    rts

print_try_count:
    inc try_count
    lda try_count
    jsr print_binary   
    lda #" "
    jsr print_char_serial
    rts 

number_played:
    jsr check_win      

    lda #" "
    jsr print_char_serial

    jsr give_hints 
    jsr print_hints
    
    lda #13
    jsr print_char_serial

    jsr print_try_count
    rts

print_hints:
    ldx #0
    lda number_almost_correct_numbers
    beq should_correct_loop 
    lda #"-"
almost_correct_loop:
    jsr print_char_serial
    inx
    cpx number_almost_correct_numbers
    bne almost_correct_loop
    ldx #0
    lda #" "
    jsr print_char_serial
should_correct_loop:
    lda number_correct_numbers
    beq end_print_hints
    lda #"+"
correct_loop:
    jsr print_char_serial
    inx
    cpx number_correct_numbers
    bne correct_loop
end_print_hints:
    rts 

give_hints:
    stz number_correct_numbers
    stz number_almost_correct_numbers
    ldx #0
check_numbers_loop:
    lda secret_number, x
    cmp received_before_cr, x
    beq correct_number
    bne wrong_number
continue_check_numbers:
    inx
    cpx lane_number
    bne check_numbers_loop
    rts    

correct_number:
    ; correct number at the correct place
    inc number_correct_numbers
    jmp continue_check_numbers
wrong_number:
    ; the number at place X is not correct
    ; is it maybe somewhere else ?
    ldy #0
wrong_number_loop:
    lda secret_number, y
    cmp received_before_cr, x 
    beq correct_another_place
    iny
    cpy lane_number
    bne wrong_number_loop
    jmp continue_check_numbers 

correct_another_place:
    inc number_almost_correct_numbers
    jmp continue_check_numbers

check_win:
    ldx #0
check_win_loop:
    lda secret_number, x
    cmp received_before_cr, x
    bne false_number
    inx
    cpx lane_number
    bne check_win_loop
    ; actually won
    jmp won

false_number:
    rts 

won:
    lda #13
    jsr print_char_serial

    ldx #>msg_won
    ldy #<msg_won
    jsr print_msg_serial    
   
    ; print the secret number
    ldx #0 
won_loop:
    lda secret_number, x
    jsr print_number_serial
    inx
    cpx lane_number
    bne won_loop
     
    lda #13
    jsr print_char_serial

    plx ; on enlève l'adresse de retour du stack
    plx 
    jsr reset ; pas sûr de moi, peut causer un stack overflow si on recommence plein de parties ? -> non car au reset on remet le stack pointer à $ff
 
init_via:
    pha
    lda #%11111111 ; tous les pins du port B sont des sorties
    sta DDRB

    lda #%11100000 ; pins 5,6,7 de A sont des sorties
    sta DDRA
    pla
    rts

init_lcd_display:
    pha
    lda #%00111000 ; init display, 8 bits, 2 lignes, 5x8 pixels par charactère
    jsr send_lcd_command
    
    lda #%00001110 ; affichage, curseur, pas de blink
    jsr send_lcd_command

    lda #%00000110 ; incrémente, pas de scroll
    jsr send_lcd_command

    lda #%00000001 ; efface l'écran
    jsr send_lcd_command
    pla
    rts

init_6551:
    pha
    lda #$00
    sta ACIA_STATUS ; soft reset

    lda #%00011111 ; N-8-1, 19200
    sta ACIA_CTRL
    
    lda #%00001011 ; tout disable
    sta ACIA_CMD
    pla
    rts

print_startmsg_lcd:
    phx
    pha
    ldx #0
loop_print_startmsg_lcd:
    lda startmsg_lcd,x
    beq print_startmsg_lcd_return
    jsr print_char_lcd
    inx
    jmp loop_print_startmsg_lcd
print_startmsg_lcd_return:
    plx
    pla
    rts

print_msg_serial:
    ; X contains the high order byte adress of the null-terminated message to be printed
    ; Y contains the low order byte
    phy
    phx
    pha
    stx message_to_print_ho
    ldx #0
    stx message_to_print_lo

loop_print_msg_serial:
    lda (message_to_print_lo),y     ; ho * 256 + lo + y (avec lo = 0)
    beq print_msg_serial_return
    jsr print_char_serial
    iny
    bne loop_print_msg_serial
    inc message_to_print_ho
    jmp loop_print_msg_serial
print_msg_serial_return:
    plx
    pla
    ply
    rts

print_number_serial:
    pha
    clc
    adc #"0"
    jsr print_char_serial
    pla
    rts


print_char_serial:
    pha
    sta ACIA_DATA
tx_wait:
    jsr tx_delay
    pla
    rts

tx_delay:
    phx
    ldx #100    ; delay because hw bug
loop_tx_delay:
    dex
    bne loop_tx_delay
    plx
    rts


;
;   LCD subroutines
;

wait_lcd:
    pha            ; push a on the stack 
    lda #%00000000
    sta DDRB
lcd_busy:
    lda #RW       ; on read pour une fois
    sta PORTA
    lda #(RW | E)
    sta PORTA
    lda PORTB
    and #%10000000 ; le premier bit nous intéresse, ça update le zero flag aussi
    bne lcd_busy

    lda #RW
    sta PORTA
    lda #%11111111
    sta DDRB
    pla             ; pull a
    rts

send_lcd_command:
    jsr wait_lcd
    sta PORTB
    lda #0         ; RS = RW = 0, toggle de E
    sta PORTA
    lda #E
    sta PORTA
    lda #0
    sta PORTA
    rts

print_char_lcd:
    pha
    jsr wait_lcd
    sta PORTB
    lda #RS        ; RS = 1, RW = 0, toggle de E
    sta PORTA
    lda #(E | RS)
    sta PORTA
    lda #RS
    sta PORTA
    pla
    rts

;
;   Print an 8 bit (or 16) binary number to decimal
;

print_binary:
    ; register A contains the number
    sta value
    pha
    lda #0
    sta value + 1
    sta message_binary_buffer

divide:
    lda #0
    sta mod10
    sta mod10 + 1
    
    clc     ; clear the carry bit
    
    ldx #16
divloop:
    ; rotate left (merci le carry bit) 
    rol value
    rol value + 1
    rol mod10
    rol mod10 + 1 

    ; a, y = dividende - diviseur
    sec         ; set the carry
    lda mod10
    sbc #10      ; soustraction avec carry
    tay         ; on sauvegarde le low byte dans le registre Y
    lda mod10 + 1
    sbc #0     
    bcc ignore_result   ; branch if carry clear
    sty mod10
    sta mod10 + 1

ignore_result:
    dex         ; decrement x
    bne divloop ; zero flag not set
    
    rol value
    rol value + 1    
    
    lda mod10
    clc
    adc #"0"
    jsr push_char
    
    ; on continue tant que value != 0   
    lda value
    ora value + 1 ; bitwise or
    bne divide

    ldx #0
find_nmb_zeros:
    lda message_binary_buffer,x
    beq zero_reached
    inx
    jmp find_nmb_zeros
zero_reached:
    ; register X contains address of first 0
    txa
    cpx #3
    beq continue_print    
    inx
    lda #"0"
    phx
    jsr push_char
    plx
    jmp zero_reached 
    
continue_print:

    ldx #0 

print:
    lda message_binary_buffer,x
    beq return
    jsr print_char_serial
    inx
    jmp print 
   
push_char:
    pha
    ldy #0  ; index

char_loop:
    lda message_binary_buffer,y
    tax
    pla
    sta message_binary_buffer,y 
    iny
    txa
    pha
    bne char_loop
    
    pla
    sta message_binary_buffer,y    ; null char
    
    rts

return:
    pla
    rts

startmsg_serial: .byte "Connection etablie"
                 .byte 13
                 .byte "====MasterMind V1.0===="
                 .asciiz 13

startmsg_lcd: .asciiz "MasterMind  V1.0"

msg_pick_lane_number: .byte "Veuillez choisir un nombre de ligne, entre "
                      .byte MIN_LANES + key_zero
                      .byte " et "
                      .byte MAX_LANES + key_zero
                      .asciiz 13

msg_picked_lane_number: .asciiz "Vous avez choisi ce nombre de ligne : "

msg_picked_wrong_number: .byte "Le nombre que vous avez choisi est incorrect" 
                              .asciiz 13

msg_pick_max_number:  .byte "Veuillez choisir le nombre maximal qui pourra etre joue, entre "
                      .byte MIN_USABLE_NUMBER + key_zero
                      .byte " et "
                      .byte MAX_USABLE_NUMBER + key_zero
                      .asciiz 13

msg_picked_secret_number: .asciiz "Un nombre secret a ete choisi, vous pouvez commencer a jouer : ", 13

msg_picked_max_number: .asciiz "Vous avez choisi cet interval de chiffres : 0 -> "

msg_won: .asciiz "Felicitations, vous avez trouve le nombre mystere qui est : "
