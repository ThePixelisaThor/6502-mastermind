# 6502 Mastermind

Unoptimized mastermind program, made for a 6502 computer.
This should work out of the box if you made Ben Eater's 6502 (https://eater.net/6502).

## Features

You can pick the number of numbers that you'll have to guess.  
You can pick the range of possible numbers.  
If you've send a character you didn't want to send, you can send Backspace and change it.  

![Running the mastermind](assets/mastermind_french.png)